#ifndef QUEUE_H
#define QUEUE_H


/* a queue contains positive integer values. */
typedef struct queue
{
	int* _elements;
	int _amount;
	int _last;
	int _first;
} queue;

void initQueue(queue* q, unsigned int size);//function initiates new q
void cleanQueue(queue* q);//function frees all allocated memory of q
bool isFull(queue* q);//returns if queue is full
bool isEmpty(queue* q);//returns if queue is empty

void enqueue(queue* q, unsigned int newValue);//pushs val into stack
int dequeue(queue* q); // return element in top of queue, or -1 if empty

#endif /* QUEUE_H */