#include"queue.h"
#include<iostream>

//function initiates new q
void initQueue(queue* q, unsigned int size)
{
	q->_elements = new int[size];
	q->_amount = size;
	q->_last = -1;
	q->_first = -1;
}

//function frees all allocated memory of queue
void cleanQueue(queue* q)
{
	delete[] q->_elements;
	delete q;
}

//returns if q is empty
bool isEmpty(queue* q)
{
	return (q->_first == q->_last && q->_last == -1);
}

//returns if queue is full
bool isFull(queue* q)
{
	return ((q->_last + 1) % q->_amount == q->_first);
}

//puts new num in queue
void enqueue(queue* q, unsigned int newValue)
{
	if (isFull(q))
	{
		std::cout << "Error: Queue is Full\n";
		return;
	}
	else if (isEmpty(q))
	{
		q->_first = q->_last = 0;
	}
	else
	{
		q->_last = (q->_last + 1) % q->_amount;
	}
	q->_elements[q->_last] = newValue;
}

//pops val in q (returns -1 if empty)
int dequeue(queue* q)
{
	int temp;
	if (isEmpty(q))
	{
		temp = -1;
	}
	else if (q->_first == q->_last)
	{
		temp = q->_elements[q->_last];
		q->_last = q->_first = -1;
	}
	else
	{
		temp = q->_elements[q->_first];
		q->_first = (q->_first + 1) % q->_amount;
	}
	return temp;
}