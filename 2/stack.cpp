#include<iostream>
#include "linkedList.h"
#include "stack.h"

void push(stack* s, unsigned int element)
{
	s->head = addNode(s->head);
	s->head->val = element;
	s->size++;
}

int pop(stack* s)
{
	int returnVal = 0;
	node* temp = s->head;
	s->size--;
	returnVal = temp->val;
	s->head = s->head->next;
	delete temp;
	return returnVal;
}
// Return -1 if stack is empty

void initStack(stack* s)
{
	s->head = NULL;
	s->size = 0;
}

void cleanStack(stack* s)
{
	node* temp = s->head;
	node* prev;
	do
	{
		prev = temp;
		temp = temp->next;
		delete prev;
	} while (temp->next != NULL);

	delete s;
}