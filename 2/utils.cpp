#include<iostream>
#include "linkedList.h"
#include "stack.h"
#include "utils.h"

void reverse(int* nums, unsigned int size)
{
	stack* s = new stack;
	unsigned int i = 0;
	while (i < size)
	{
		push(s, nums[i]);
		i++;
	}
	i = 0;
	while (i < size)
	{
		nums[i] = pop(s);
		i++;
	}
}

int* reverse10()
{
	int* arr = new int[10];
	int i = 0;
	while (i < 10)
	{
		std::cin >> arr[i];
		i++;
	}
	reverse(arr, 10);
	return arr;
}