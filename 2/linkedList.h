#ifndef LINKED_LIST
#define LINKED_LIST

typedef struct node{
	node* next;
	unsigned int val;
}node;

void freeAll(node* H);
node* addNode(node* H);
node* removeNode(node* H);


#endif // LINKED_LIST

